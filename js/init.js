window.onload = function()
{ 
    Presentation.init();
    
    modalsWindow();
};

window.onresize = function()
{
    Presentation.init();
};

window.onscroll = function()
{
    Presentation.detectAndScroll();
};
