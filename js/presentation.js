/**
 * Javascript animated presentation
 * Mostly based on CSS transitions 
 * 
 * DOM must be : 
 * <div id="page">
 *      <section id="introduction" tabindex="1" data-anim="slide" data-bkgimage="01.jpg">
 *          <article>
 *              <h1>Title of this slide</h1>
 *              <p tabindex="5" data-anim="fadein">Content of this paragraph.</p>
 *          </article>
 *      <section>
 *      
 *      <section id="issue-1" tabindex="20" data-anim="slide" data-bkgimage="01.jpg">
 *          <h2>Title that arrived with the slide</h2>
 *          <article>
 *              <h1>Title of this slide</h1>
 *              <p tabindex="5" data-anim="cross-fade">Content of this paragraph.</p>
 *          </article>
 *      <section>
 * </div>
 * 
 * Each slides is defned by a <section> tag. Which has attributes: 
 *  - id to specify the anchor name (used in the URL)
 *  - tabindex to specify the anchor name (used in the URL)
 *  - data-anim for slide animation
 *  - Define the background :
 *      - data-bkgimage - background-image (with the path to the images folder)
 *      - data-bkggradient - background gradient used in CSS
 *      - data-bkgcolor - color hexadecimal or rgb() or rgba()
 *  
 * The <article> tag has the content that will do a fadeIn when appearing. 
 * Contents outside the <article> tag will slide in with the slide.
 * 
 * Each element that has an tabindex attribute will be set in the slideshow as a step.
 * The data-anim attribute will define the type of animation for it's apperance. 
 * It's value could be :
 *  - "fadein" : the content appears by changing it's opacity.
 *  - "cross-fade" : the content appears by changing it's opacity and make others disappear. 
 *  - "porgress" : the <progress> tag makes an animated progression. It has to be set to the container of the prgress bar
 *  
 * The menu has to be an HTML list in a <nav> tag which needs to be in an <aside> tag.
 * It needs to contains the exact same number of slides. 
 * <aside>
 *     <nav>			
 *         <ul class="nav show">
 *             <li><a href="">Intriduction</a></li>
 *             <li><a href="">How it works</a></li>
 *             <li><a href="">Examples</a></li>
 *             <li><a href="">Do it by yourself</a></li>
 *             <li><a href="">Conclusion</a></li>
 *         </ul>
 *     </nav>
 * </aside> 
 */

class Presentation
{
    static type     = 'slide'; // if not define, slides are doing cross fade when changing
    static wHeight  = null;
    static wWidth   = null;
    static anims    = {};
    static curAnim  = 0;
    static curSlide = 0;
    
    static indexAnim= null;
    static nbAnims  = 0;
    static slides   = null;
    static nbParts  = 0;
    
    static menus    = null;
        
    static progressH= 0; // Total of heights gone through
    static initDone = false;
    
    static eventCall= false;
    
    static init()
    {
        Presentation.eventCall  = true;
        Presentation.wHeight    = window.innerHeight;
        Presentation.wWidth     = window.innerWidth;
        Presentation.indexAnim  = document.querySelectorAll( 'div#page *[tabindex]' );
        Presentation.nbAnims    = Presentation.indexAnim.length;
        Presentation.slides     = document.querySelectorAll( 'div#page > section' );
        Presentation.nbParts    = Presentation.slides.length;
        
        Presentation.menus      = document.querySelectorAll( 'ul.nav li a' );
                        
        this.setElements(); 
        
        Presentation.goToAnchor();   
        
        Presentation.initDone = false;     
        
        if( !Presentation.initDone )
        {            
            Presentation.initDone = true;
            
            this.setEvents(); 
        }
    }
    
    
    static setElements()
    {  
        let indexsl      = 0;
        
        let scrollMargin = Presentation.wHeight / 1;
        
        Array.prototype.forEach.call( Presentation.indexAnim, function( tabAnim, index )
        {                    
            let slidescrollstart    = 0;
            let slidescrollend      = 0;
            let indexslide          = 0;
            
            if( tabAnim.dataset.anim === 'slide' ) // Organize slides with a fixed position
            {
                tabAnim.style.position  = 'fixed';
                tabAnim.style.top       = ( indexsl * Presentation.wHeight ) + 'px';
                indexslide              = indexsl;
                slidescrollstart        = indexsl * Presentation.wHeight - scrollMargin / 2;
                slidescrollend          = indexsl * Presentation.wHeight + scrollMargin / 2;
                indexsl++;
                
                tabAnim.querySelector('article').style.transitionDuration = '0.5s';
                tabAnim.querySelector('article').style.transitionDelay = '0.65s';
                tabAnim.querySelector('article').style.transitionProperty = 'all';
            }
                
            Presentation.anims[ index ] = 
            {
                'index':index, 
                'indexslide':indexslide, 
                'anim':tabAnim.dataset.anim, 
                'obj':tabAnim, 
                'slidescrollstart':slidescrollstart, 
                'slidescrollend':slidescrollend
            };
            
            tabAnim.style.transitionDuration = '0.5s';
            tabAnim.style.transitionProperty = 'all';
            
            Presentation.setMenuSelected();
        });
        
        Presentation.purgeForwardSlides(); 
        
        document.querySelector( 'body > aside > nav' ).style.height = Presentation.wHeight + 'px';
        
        document.querySelector( 'ul.nav' ).classList.remove( 'show' );
        
        document.querySelector( 'div#page' ).style.height = ( Presentation.wHeight * Presentation.nbParts ) + 'px';

        Presentation.animSlide();
        
        document.querySelector( 'body' ).style.transition = 'all 0.5s';
    }
    
    
    static setEvents()
    {        
        document.querySelector('#left').onclick = function()
        {
            Presentation.prevSlide();
        };
                
        document.querySelector( '#right' ).onclick = function()  ///// Arrow right /////
        {
            Presentation.nextSlide();
        };
        
        window.onkeydown = function( e )
        {
            if( e.keyCode === 37 )  // left key pressed
            {
               Presentation.prevSlide();
                              
               return false;
            }
            
            if( e.keyCode === 39 )   // right key pressed
            {
               Presentation.nextSlide();
                              
               return false;
            }
            
            if( e.keyCode === 9 )   // tab key pressed
            {
               Presentation.nextSlide();
                              
               return false;
            }
        };
        
        
        ///// Menu /////
        Array.prototype.forEach.call( Presentation.menus, function( menu )
        {
            menu.onclick = function( e )
            {
                e.preventDefault();
                
                let clickedMenu = this;
                
                Array.prototype.forEach.call( Presentation.menus, function( m, index )
                {
                    if( m === clickedMenu )
                    {
                        Presentation.setCurrentInfos( index );
                        
                        Presentation.setArrows();
                    }
                });
            };
            
        });
        
        
        ///// Back to Top /////
        Array.prototype.forEach.call( document.querySelectorAll( 'a.top' ), function( a )
        {
            a.onclick = function( e )
            {
                e.preventDefault();

                Presentation.curAnim = 0;

                Presentation.curSlide = 0;

                Presentation.animSlide(); 

                Presentation.setArrows();
            };
        });
    }
    
    
    static setCurrentInfos( index )
    {
        for( const a in Presentation.anims )
        {
            let anim = Presentation.anims[ a ];
            
            if( anim.indexslide === index && anim.anim === 'slide' )
            { 
                Presentation.curAnim = anim.index;

                Presentation.curSlide = anim.indexslide;

                Presentation.animSlide();
            }
        }
    }
    
    
    static setMenuSelected()
    {
        Array.prototype.forEach.call( Presentation.menus, function( menu, index )
        {
            if( index === Presentation.curSlide )
            {
                menu.parentNode.classList.add( 'actif' );
            }
            else
            {
                menu.parentNode.classList.remove( 'actif' );
            }
        });
    }
    
    static setArrows()
    {
        document.querySelector('#left').classList.remove('disabled');
        document.querySelector('#right').classList.remove('disabled');
        
        Array.prototype.forEach.call( Presentation.slides, function( slide, index )
        {
            if( index === Presentation.curSlide )
            {
                if( Presentation.curSlide === 0 )
                {
                    document.querySelector('#left').classList.add('disabled');
                }
                if( Presentation.curSlide === ( Presentation.nbParts - 1 ) )
                {
                    document.querySelector('#right').classList.add('disabled');
                }
                
            }
        });
        
        document.querySelector('footer span').innerHTML = ( Presentation.curSlide + 1 ) + '/' + Presentation.nbParts;
    }
    
    static animSlide()
    {
        Array.prototype.forEach.call( Presentation.slides, function( slide, index )
        {
            if( Presentation.curSlide === index )
            {
                document.querySelector( 'div#page > section:nth-child(' + ( index + 1 ) + ') article' ).classList.add('display');
                
                let randomId = new Date().getTime();
                
                if( Presentation.anims[ Presentation.curAnim ].obj.dataset.bkgimage )
                {
                document.querySelector('html').style.backgroundImage = "url('" + Presentation.anims[ Presentation.curAnim ].obj.dataset.bkgimage + "?random=" + randomId + "')";
                document.querySelector('html').style.backgroundColor = '#ffffff';
                document.querySelector('body').style.backgroundImage = "url('" + Presentation.anims[ Presentation.curAnim ].obj.dataset.bkgimage + "?random=" + randomId + "')";
                }
                else if( Presentation.anims[ Presentation.curAnim ].obj.dataset.bkggradient )
                {
                document.querySelector('html').style.backgroundImage = Presentation.anims[ Presentation.curAnim ].obj.dataset.bkggradient;
                document.querySelector('html').style.backgroundColor = '#ffffff';            
                document.querySelector('body').style.backgroundImage = Presentation.anims[ Presentation.curAnim ].obj.dataset.bkggradient;
                }
                else if( Presentation.anims[ Presentation.curAnim ].obj.dataset.bkgcolor )
                {
                document.querySelector('html').style.backgroundImage = 'none';
                document.querySelector('html').style.backgroundColor = Presentation.anims[ Presentation.curAnim ].obj.dataset.bkgcolor ;               
                document.querySelector('body').style.backgroundImage = 'none';               
                document.querySelector('body').style.backgroundColor = Presentation.anims[ Presentation.curAnim ].obj.dataset.bkgcolor ;
                }
            }
            else
            {
                document.querySelector( 'div#page > section:nth-child(' + ( index + 1 ) + ') article' ).classList.remove('display');
            }
            
            if( Presentation.type === 'slide' )
            {
                slide.style.top = ( ( index - Presentation.curSlide ) * Presentation.wHeight ) + 'px';
            }
            else
            {
                slide.style.top = '0px';
            }
        });
        
        Presentation.setMenuSelected();
        
        Presentation.purgeForwardSlides();
                
        if( Presentation.initDone )
        {
            Presentation.progressH = Presentation.curSlide * Presentation.wHeight;

            Presentation.eventCall = true;

            Presentation.setAnchor();

            window.scrollTo( 0, Presentation.progressH ); 
        }
    }
    
    
    static prevSlide()
    {
        if( Presentation.curSlide > 0 )
        {
            Presentation.curSlide--;
            
            Presentation.setCurrentInfos( Presentation.curSlide );
            
            document.querySelector('#left').classList.add('hover');
            
            setTimeout( function(){ document.querySelector('#left').classList.remove('hover'); Presentation.setArrows(); }, 300 );
        }
    }


    static nextSlide()
    {
        if( Presentation.curAnim < ( Presentation.nbAnims - 1 ) ) 
        {
            Presentation.curAnim++;
            
            let newestAnim = Presentation.anims[ Presentation.curAnim ];
            
            if( newestAnim.anim === 'slide' )
            {            
                Presentation.curSlide++;
                    
                Presentation.animSlide();
            }
            else 
            {
                Presentation.animContent( newestAnim );
            }
            
            document.querySelector('#right').classList.add('hover');
            
            setTimeout( function(){ document.querySelector('#right').classList.remove('hover'); Presentation.setArrows(); }, 300 );
        }
    };    
    
    
    
    static animContent( newestAnim )
    {        
        if( newestAnim.anim === 'fadein' )
        {
            newestAnim.obj.classList.add('display');

            Presentation.setAnchor(); 
        }
        else if( newestAnim.anim === 'cross-fade' )
        {
            for( const a in Presentation.anims )
            {    
                let anim = Presentation.anims[ a ];

                if( anim.anim !== 'slide' ) // Organize slides with a fixed position
                {
                    if( newestAnim.index === anim.index )
                    {
                        anim.obj.classList.add('display');
                    }
                    else
                    {
                        anim.obj.classList.remove('display');
                    }
                }
            };

            Presentation.setAnchor(); 
        }
        else if( newestAnim.anim === 'progress' )
        {
            let progress = newestAnim.obj.querySelector('progress');
            
            let value = progress.value;
            
            let progressValue = 0;
            
            progress.value = progressValue;
            
            newestAnim.obj.classList.add('display');
            
            let animLoad = setInterval( function()
            {
                if( progressValue >= value )
                {
                     clearInterval( animLoad );
                }
                else
                {
                    progressValue += 1;

                    progress.value = progressValue;
                }
            }, 20 );

            Presentation.setAnchor(); 
        }
    }
    
    
    // Cleans (make disappear elements) from a specific slide until the last
    static purgeForwardSlides = function()
    { 
        let slideToGo = false;
        
        for( var a in Presentation.anims )
        {
            if( Presentation.curSlide === Presentation.anims[a].index )
            { 
                slideToGo = true; 
            }
            
            if( slideToGo )
            {
                if( Presentation.anims[a].anim === 'fadein' || Presentation.anims[a].anim === 'cross-fade' || Presentation.anims[a].anim === 'progress' )
                {
                    if( Presentation.anims[a].obj )
                    {
                       Presentation.anims[a].obj.classList.remove('display');
                    }
                }
            }
        }
    };
    
    
    
    static setAnchor()
    {
        if( Presentation.initDone ) // Because it is link to every slide animation. Make sure that it's not done when the page is laoding the first time
        {
            let slide = 0;
            
            let slideAnim = 0;
                
            for( const a in Presentation.anims )
            {    
                let anim = Presentation.anims[ a ];
                
                if( anim.indexslide > 0 )
                {
                    slide = anim.indexslide;
                    
                    slideAnim = 0;
                }

                if( anim.index === Presentation.curAnim )
                {            
                    const newest = document.querySelector( 'div#page > section:nth-child(' + ( slide + 1 ) + ')' );

                    window.location.hash = newest.id + ( ( slideAnim !== 0 ) ? '-' + slideAnim : '' );
                }
                
                slideAnim++;
            }
        }
    }
    
    
    static goToAnchor()
    {   
        let url = window.location.hash.substr(1);
        
        let urlParts = url.split('-');
        
        let urlSlide = ( urlParts.length > 0 ) ? urlParts[ 0 ] : url; 
                
        if( urlSlide.length >= 1 && document.querySelector( 'div#page > section#' + urlSlide ) )
        {
            let parts = document.querySelectorAll( 'div#page > section' );
            
            Array.prototype.forEach.call( parts, function( part, index )
            {
                if( part.id === urlSlide )
                {
                    Presentation.setCurrentInfos( index );
                    
                    Presentation.setArrows();
                    
                    if( urlParts.length > 1 )
                    {
                        for( const a in Presentation.anims )
                        { 
                            let anim = Presentation.anims[ a ];
                            
                            if( anim.obj === part ) // The current slide
                            {
                                for( let i = 0; i < urlParts[ 1 ]; i++ )
                                {
                                    Presentation.curAnim = anim.index + i + 1;
                                    
                                    let newestAnim = Presentation.anims[ Presentation.curAnim  ];

                                    Presentation.animContent( newestAnim );
                                }
                            }
                        }
                    }
                }
            });            
        };
    }
    
    
    static detectAndScroll()
    {
        let scrollPage = window.scrollY || window.pageYOffset;
        
        if( Presentation.initDone )
        {
            if( Math.round( scrollPage ) >= Presentation.wHeight )
            {
                document.querySelector('footer a').classList.add('display');
            }
            else
            {
                document.querySelector('footer a').classList.remove('display');
            }
            
            if( !Presentation.eventCall )
            {
                Presentation.switchSlide( scrollPage );
            }
            else
            {
                setTimeout( function(){ Presentation.eventCall = false; }, 700 );
            }
        }
    }
    
    
    static switchSlide( scrollPage )
    {
        for( const a in Presentation.anims )
        { 
            let anim = Presentation.anims[ a ];
            
            if( anim.anim === 'slide' && anim.indexslide !== Presentation.curSlide && anim.slidescrollstart < scrollPage && anim.slidescrollend > scrollPage )
            {
                Presentation.setCurrentInfos( anim.indexslide );
                
                Presentation.setArrows();
            }
        }
        
    }
};