let modalsWindow = function()
{
    let modals = document.querySelectorAll( '[data-modal="true"]' );
    
    Array.prototype.forEach.call( modals, function( modal )
    {
        modal.onclick = function( e )
        {
            e.preventDefault();
            
            let body = document.querySelector('body');
            
            if( body.classList.contains('modal') )
            {
                body.classList.remove('modal');
                
                document.querySelector('div[class="modal"] main').innerHTML = '';
            }
            else
            {
                body.classList.add('modal');
                
                let clone = document.importNode( modal.querySelector('template').content, true );

                let cloneContents = clone.querySelector('div');

                document.querySelector('div[class="modal"] main').innerHTML = cloneContents.innerHTML; 
            }
        };
    });
    
    document.querySelector('div[class="modal-background"]').onclick = function()
    {
        document.querySelector('body').classList.remove('modal');
                
        document.querySelector('div[class="modal"] main').innerHTML = '';
    };
    
    document.querySelector('div[class="modal"] .cross').onclick = function()
    {
        document.querySelector('body').classList.remove('modal');
                
        document.querySelector('div[class="modal"] main').innerHTML = '';
    };    
};
