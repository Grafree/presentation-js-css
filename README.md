<h1>Javascript animated presentation</h1>
<p>Mostly based on CSS transitions</p> 
 
<p>DOM must be :</p>

    <div id="page">
      <section id="introduction" tabindex="1" data-anim="slide" data-bkgimage="01.jpg">
          <article>
              <h1>Title of this slide</h1>
              <p tabindex="5" data-anim="fadein">Content of this paragraph.</p>
          </article>
      <section>
      
      <section id="issue-1" tabindex="20" data-anim="slide" data-bkgimage="01.jpg">
          <h2>Title that arrived with the slide</h2>
          <article>
              <h1>Title of this other slide</h1>
              <p tabindex="5" data-anim="cross-fade">Content of this paragraph.</p>
          </article>
      <section>
    </div>

Each slides is defned by a `<section>` tag. Which has attributes: 
- **id** to specify the anchor name (used in the URL)
- **tabindex** to specify the anchor name (used in the URL)
- **data-anim** for slide animation
- **Define the background :
  - **data-bkgimage - background-image (with the path to the images folder)
  - **data-bkggradient - background gradient used in CSS
  - **data-bkgcolor - color hexadecimal or rgb() or rgba()

  
The `<article>` tag has the content that will do a fadeIn when appearing. 
Contents outside the `<article>` tag will slide in with the slide.
 
Each element that has an tabindex attribute will be set in the slideshow as a step.
The data-anim attribute will define the type of animation for it's apperance. 
It's value could be :
- **"fadein"** : the content appears by changing it's opacity.
- **"cross-fade"** : the content appears by changing it's opacity and make others disappear. 
- **"porgress"** : the `<progress>` tag makes an animated progression. It has to be set to the container of the prgress bar

  
The menu has to be an HTML list in a `<nav>` tag which needs to be in an `<aside>` tag.
It needs to contains the exact same number of slides.

    <aside>
     <nav>			
         <ul class="nav show">
             <li><a href="">Intriduction</a></li>
             <li><a href="">How it works</a></li>
             <li><a href="">Examples</a></li>
             <li><a href="">Do it by yourself</a></li>
             <li><a href="">Conclusion</a></li>
         </ul>
     </nav>
    </aside> 